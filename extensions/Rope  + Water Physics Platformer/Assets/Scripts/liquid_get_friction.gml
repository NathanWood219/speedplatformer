///liquid_get_friction(id)
/*
    Returns the friction of the liquid
    
    ARGUMENTS
        id - real - id of liquid object
        
    RETURNS
        friction - real - higher numbers will yeild a thicker liquid
*/

    //get variables
    return argument0.liquidfriction;
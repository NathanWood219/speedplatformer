///saveAbilities()
savedRunInAir       = canRunInAir;
savedVariableJump   = canVariableJump;
savedRunOnGround    = canRunOnGround;
savedWalk           = canWalk;
savedSwim           = canSwim;
savedWallJump       = canWallJump;
savedWallSlide      = canWallSlide;
savedDive           = canDive;
savedCrouch         = canCrouch;
savedFloat          = canFloat;

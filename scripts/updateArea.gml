///updateArea()
Area = abs(ds_grid_get(WorldMap, levelX, levelY));

if(Area == 7 || Area == 9 || Area == 11) {
    LightAmbience = 0.2;
} else {
    LightAmbience = 0.7;
}

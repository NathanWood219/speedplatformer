if(keyboard_check_pressed(vk_escape)) {
    game_end();
}

if(keyboard_check_pressed(ord('R'))) {
    room_restart();
}

if(keyboard_check_pressed(vk_f1) && room_previous(room) != -1) {
    room_goto_previous();
}

if(keyboard_check_pressed(vk_f2) && room_next(room) != -1) {
    room_goto_next();
}

if(keyboard_check_pressed(vk_f10)) {
    game_restart();
}

if(keyboard_check_pressed(vk_f11)) {
    var filename;
    
    filename = "Screenshots\" + string(levelX) + "," + string(levelY);
        
    if!(file_exists(filename + ".png")) {
        screen_save("Screenshots\" + string(levelX) + "," + string(levelY) + ".png");
    } else {
        var num = 1;
        
        while(file_exists(filename + " (" + string(num) + ").png")) {
            num++;
        }
        
        screen_save("Screenshots\" + string(levelX) + "," + string(levelY) + " (" + string(num) + ").png");
    }
}

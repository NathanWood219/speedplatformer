///movingObjLiquid()

if(!physics) {
    exit;
}

//find closest liquid box
var obj  = instance_nearest_bbox(obj_water);

//collide with liquid
lqd  = place_meeting(x, y, obj) && !place_meeting(x, y, obj_water_surfmask);
slqd = place_meeting(x, y, obj_water_surfmask); 

// Jump in water
if (!slqd) {
    if (lqd != prvlqd) {
        //realise current liquid state
        prvlqd = lqd;
        
        //apply manipulation to liquid
        liquid_apply_force_x(obj, x, vspd * 0.5);
        liquid_splash_create(obj, x, liquid_point_y(obj, x), -abs(vspd * 0.5), 16, 4, 4, 0.25, c_white, 0.5, bm_normal);
        
        //speed up on liquid exit/slowdown on entrace
        if (!lqd) {
            vspd *= 1.2;
        } else {
            vspd *= 0.25; 
        }
    }
}

// Jump in shallow water
if (slqd != sprvlqd) {
    //realise current liquid state
    sprvlqd = slqd;
}    

// Walk in shallow water
if (slqd) {
    if (hspd != 0) {
        liquid_apply_force_x(obj, x, hspd * 0.4);
        if (--splashdelay <= 0) {
            splashdelay = 6;
            liquid_splash_create(obj, x, liquid_point_y(obj, x), -abs(hspd * 0.4), 4, 4, 4, 0.25, c_white, 0.5, bm_normal); 
        }       
    }
    
    if (vspd != 0) {
        liquid_apply_force_x(obj, x, vspd * 0.5);
        liquid_splash_create(obj, x, liquid_point_y(obj, x), -abs(vspd * 0.5), 4, 4, 4, 0.25, c_white, 0.5, bm_normal);        
    }    
}

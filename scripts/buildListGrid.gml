///buildListGrid(string grid1, string grid2) - returns the newly built list grid; must be a rectangle
// Specialized version: converts each token to its digits and negates the value

globalvar gridArray, grid, list;

// Get length of grid
gridArray1 = tokenizer(argument0, true);
gridArray2 = tokenizer(argument1, true);
grid = ds_grid_create(array_height_2d(gridArray1), array_height_2d(gridArray1));

for(i = 0; i < array_height_2d(gridArray1); i++) {
    for(k = 0; k < array_length_2d(gridArray1, i); k++) {
        list = ds_list_create();
        ds_list_add(list, -real(gridArray1[i, k]), real(gridArray2[i, k]), 0, 0, 0, 0);
        ds_grid_add(grid, k, i, list);
        ds_list_destroy(list);
    }
}

return grid;

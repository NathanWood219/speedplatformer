// Wall to left  --> return -1
// Wall to right --> return 1
// Otherwise     --> return 0

if(checkWall(x-2, y)) {
    return -1;
} else if(checkWall(x+2,y)) {
    return 1;
}

return 0;

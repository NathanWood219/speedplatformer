///distanceToObject(x, y, other.x, other.y)

return sqrt(power(argument0 - argument2, 2) + power(argument1 - argument3, 2));

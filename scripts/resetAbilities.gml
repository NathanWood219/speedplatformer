///resetAbilities()
canRunInAir         = savedRunInAir;
canVariableJump     = savedVariableJump;
canRunOnGround      = savedRunOnGround;
canWalk             = savedWalk;
canSwim             = savedSwim;
canWallJump         = savedWallJump;
canWallSlide        = savedWallSlide;
canDive             = savedDive;
canCrouch           = savedCrouch;
canFloat            = savedFloat;

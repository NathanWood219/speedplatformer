///particleStep()
// Nathan Wood
// 2 / 19 / 16
// Put this in the STEP event of 'obj_particle'

// Applying gravity and horizontal friction
if(!place_meeting(x, y + 1, obj_wall)) {
    vspd += GRAVITY;
} else {
    vspd = 0;
    
    if(hspd != 0) {
        hspd = approach(hspd, 0, fric);
    }
}

// Moving based on velocities
x += hspd;
y += vspd;

// Rotating the image
if(hspd != 0) {
    angle -= hspd / abs(hspd / 2.5);
} else if(vspd != 0) {
    angle -= vspd / abs(vspd / 2.5);
}

// Fading out and destroying
if(hspd == 0 && vspd == 0) {
    if(alpha > 0) {
        alpha -= FADE_RATE;
    } else {
        instance_destroy();
    }
}

// Destroying if outside the room
if(x > room_width || x < 0 || y > room_height || y < 0) {
    instance_destroy();
}

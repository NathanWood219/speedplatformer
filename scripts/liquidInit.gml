///liquidInit(string liquidType)

if(argument0 == "lake") {
    WAVE        = 1;
    WAVE_AMOUNT = 2;
    WAVE_SPEED  = 1;
    COLOR1      = c_aqua;
    COLOR2      = c_aqua;
    ALPHA1      = 0.2;
    ALPHA2      = 0.3;
} else if(argument0 == "ocean") {
    WAVE        = 1;
    WAVE_AMOUNT = 10;
    WAVE_SPEED  = 2;
    COLOR1      = c_aqua;
    COLOR2      = c_aqua;
    ALPHA1      = 0.5;
    ALPHA2      = 0.6;
} else if(argument0 == "cave") {
    WAVE        = 0;
    WAVE_AMOUNT = 0;
    WAVE_SPEED  = 0;
    COLOR1      = c_teal;
    COLOR2      = c_teal;
    ALPHA1      = 0.3;
    ALPHA2      = 0.4;
}

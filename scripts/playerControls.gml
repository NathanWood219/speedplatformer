///playerControls(double accel, double fric)

accel = argument0;
fric = argument1;

if(crouching && canTurnLever) {
    canTurnLever = false;
}

if (((on_ground && canRunOnGround) || (!on_ground && canRunInAir)) && (keyboard_check_direct(key_right) || gamepad_axis_value(device,dev_haxis)>dev_h)) {
    //Running right

    //First add friction if currently running left
    if(hspd < 0) {
        hspd = approach(hspd, 0, fric);
        movement = 2;
    }
    
    if(!on_ground) {
        if(touching_wall == -1 && stickTimer == STICK_TIME + 1) {
            canMove = false;
            stickTimer = 0;
        }
        
        if(!canMove) {
            if(stickTimer < STICK_TIME) {
                stickTimer++;
            } else {
                stickTimer = STICK_TIME + 1;
                canMove = true;
            }
        }
    } else {
        canMove = true;
        stickTimer = STICK_TIME + 1;
    }
    
    if(canMove) {
        if(in_water) {
            hspd = approach(hspd, S_WATER_MAX_H, accel);
            movement = 5;
        } else if(canWalk && (keyboard_check_direct(key_walk) || gamepad_button_check(device,dev_walk))) {
            hspd = approach(hspd, S_MAX_H, accel);
            movement = 3;
        } else {
            hspd = approach(hspd, S_MAX_RUN, accel);
            movement = 4;
        }
    }
} else if (((on_ground && canRunOnGround) || (!on_ground && canRunInAir)) && (keyboard_check_direct(key_left) || gamepad_axis_value(device,dev_haxis)<-dev_h)) {
    //Running left

    //First add friction if currently running right
    if(hspd > 0) {
        hspd = approach(hspd, 0, fric);
        movement = 2;
    }
    
    if(!on_ground) {
        if(touching_wall == 1 && stickTimer == STICK_TIME + 1) {
            canMove = false;
            stickTimer = 0;
        }
        
        if(!canMove) {
            if(stickTimer < STICK_TIME) {
                stickTimer++;
            } else {
                stickTimer = STICK_TIME + 1;
                canMove = true;
            }
        }
    } else {
        canMove = true;
        stickTimer = STICK_TIME + 1;
    }
        
    if(canMove) {
        if(in_water) {
            hspd = approach(hspd, -S_WATER_MAX_H, accel);
            movement = 5;
        } else if(canWalk && (keyboard_check_direct(key_walk) || gamepad_button_check(device,dev_walk))) {
            hspd = approach(hspd, -S_MAX_H, accel);
            movement = 3;
        } else {
            hspd = approach(hspd, -S_MAX_RUN, accel);
            movement = 4;
        }
    }

} else if(!wjumping) {
    //Stopping

    if(abs(hspd)>0) {
        hspd = approach(hspd, 0, fric);
        movement = 1;
    } else {
        movement = 0;
    }
}


if(on_ground) {
    candj = true;
    canflt = true;
    floating = false;
    can_jump = true;
    jump_timer = 2;
    
    //Crouching
    if(canCrouch && (keyboard_check_direct(key_crouch) || gamepad_button_check(device, dev_crouch))) {
        crouching = true;
    }

} else {
    crouching = false;
    
    //Gravity
    if(floating) {
        vspd = approach(vspd, S_FLOAT_MAX, S_WATER_GRAVITY);
    } else if(in_water) {
        vspd = approach(vspd, S_WATER_MAX_V, S_WATER_GRAVITY);
    } else if(canWallSlide && touching_wall != 0) {
        vspd = approach(vspd, S_SLIDE_MAX, S_GRAVITY);
    } else {
        vspd = approach(vspd, S_MAX_V, S_GRAVITY);
    }
    
    //Walljumping
    if(!in_water && canWallJump && canwj && touching_wall!=0 && (keyboard_check_pressed(key_jump) || gamepad_button_check_pressed(device,dev_jump))) {
        
        //if(in_water) {
        //    hspd = S_WALLJUMP_H * wjumpDir;
        //    vspd = S_WALLJUMP_V / 2;
        //} else {
            hspd = S_WALLJUMP_H * wjumpDir;
            vspd = S_WALLJUMP_V;
        //}
        jumping = true;
        wjumping = true;
        alarm[0] = 10;
    }
    
    // Floating
    if(canflt && canFloat && vspd >= 0 && !can_jump && (keyboard_check_direct(key_float) || gamepad_button_check(device,dev_float))) {
        floating = true;
    }
    
    //Diving
    if(canDive && (keyboard_check_direct(key_dive) || gamepad_button_check(device, dev_dive))) {
        if(in_water) {
            if(vspd < S_WATER_DIVE_MAX) {
                vspd += S_DIVE_ACCEL;
            } else {
                vspd = S_WATER_DIVE_MAX;
            }
            
            // Add splash because diving
            liquid_splash_create(-1, x, y, -abs(vspd * 0.25), 16, 4, 4, 0.25, c_white, 0.5, bm_normal);
            
        } else {
            if(vspd < S_DIVE_MAX) {
                vspd += S_DIVE_ACCEL;
            } else {
                vspd = S_DIVE_MAX;
            }
        }
    }

    /*
    //Double jumping
    if ((keyboard_check_pressed(key_jump) || gamepad_button_check_pressed(device,dev_jump)) && candj) {
        candj = false;
        vspd = S_DJUMP_SPEED;
    }
    */
}

// Disabling wallstick
if(touching_wall == 0) {
    stickTimer = STICK_TIME + 1;
    canMove = true;
}

// Ending jumping state
if(jumping && vspd >= 0) {
    jumping = false;
}

//Ending floating
if(floating && !on_ground && (keyboard_check_released(key_float) || gamepad_button_check_released(device, dev_float))) {
    canflt = false;
    floating = false;
}

//Ending crouching
if(!keyboard_check_direct(key_crouch) && !gamepad_button_check(device, dev_crouch)) {
    crouching = false;
    canTurnLever = true;
}

//Swimming
if(in_water && canSwim && (keyboard_check_pressed(key_jump) || gamepad_button_check_pressed(device,dev_jump))) {
    vspd = S_WATER_SWIM;
}

//Jumping
if(can_jump && canVariableJump && (keyboard_check_pressed(key_jump) || gamepad_button_check_pressed(device,dev_jump))) {
    if(in_water) {
        vspd = S_JUMP_SPEED / 1.5;
    } else {
        vspd = S_JUMP_SPEED;
    }
    
    jumping = true;
    can_jump = false;
}

// Managing jump_timer
if(jump_timer == 0 && can_jump) {
    can_jump = false;
}

if(jump_timer > 0) {
    jump_timer--;
}

//Variable jumping
if(canVariableJump && (jumping || wjumping) && (keyboard_check_released(key_jump) || gamepad_button_check_released(device,dev_jump))) {
    vspd *= 0.35;
    alarm[3] = 2;
    jumping = false;
}

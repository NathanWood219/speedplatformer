///wallBlendOld() - [OBSOLETE] checks surrounding walls and selects appropriate sprite index

if(object_index != obj_wall) {
    if(object_index == obj_slopeR) {
        sprite_index = spr_slopeR;
    } else if(object_index == obj_slopeL) {
        sprite_index = spr_slopeL;
    } else if(object_index == obj_slopeR2) {
        sprite_index = spr_slopeR2;
    }  else if(object_index == obj_slopeL2) {
        sprite_index = spr_slopeL2;
    }
    
    exit;
}

sprite_index = spr_wall;
image_speed=0;
selected=false;
    
//inside block
if(place_meeting(x+16,y,object_index)) and (place_meeting(x-16,y,object_index)) and (place_meeting(x,y+16,object_index)) and (place_meeting(x,y-16,object_index))
    {image_index=16; selected=true}

//2 sided walls
if(place_meeting(x+16,y,object_index)) and (place_meeting(x-16,y,object_index)) and !(place_meeting(x,y+16,object_index)) and !(place_meeting(x,y-16,object_index)) and (selected=false)
    {image_index=10; selected=true}
if(place_meeting(x,y+16,object_index)) and (place_meeting(x,y-16,object_index)) and !(place_meeting(x+16,y,object_index)) and !(place_meeting(x-16,y,object_index)) and (selected=false)
    {image_index=9; selected=true}
    
//1 sided walls
if(place_meeting(x+16,y,object_index)) and (place_meeting(x-16,y,object_index)) and (selected=false)
    {if(place_meeting(x,y-16,object_index)) and (selected=false)
        {image_index=11; selected=true}
    if(place_meeting(x,y+16,object_index)) and (selected=false)
        {image_index=13; selected=true}
    }
if(place_meeting(x,y+16,object_index)) and (place_meeting(x,y-16,object_index)) and (selected=false)
    {if(place_meeting(x+16,y,object_index)) and (selected=false)
        {image_index=12; selected=true}
    if(place_meeting(x-16,y,object_index)) and (selected=false)
        {image_index=14; selected=true}
    }

//corner walls
if(place_meeting(x-16,y,object_index)) and (selected=false)
    {if(place_meeting(x,y-16,object_index)) and (selected=false)
        {image_index=5; selected=true}
    if(place_meeting(x,y+16,object_index)) and (selected=false)
        {image_index=8; selected=true}
    }
if(place_meeting(x+16,y,object_index)) and (selected=false)
    {if(place_meeting(x,y-16,object_index)) and (selected=false)
        {image_index=6; selected=true}
    if(place_meeting(x,y+16,object_index)) and (selected=false)
        {image_index=7; selected=true}
    }

//3 sided walls
if(place_meeting(x,y-16,object_index)) and (selected=false)
    {image_index=1; selected=true}
if(place_meeting(x,y+16,object_index)) and (selected=false)
    {image_index=3; selected=true}
if(place_meeting(x-16,y,object_index)) and (selected=false)
    {image_index=4; selected=true}
if(place_meeting(x+16,y,object_index)) and (selected=false)
    {image_index=2; selected=true}
    
//solid 1 block wall
if(selected=false)
    {image_index=0; selected=true}

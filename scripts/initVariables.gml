///initVariables()
// Called in a setup room before the game starts

// Saving
globalvar ShouldSaveGame;

ShouldSaveGame = false;

// Keyboard
globalvar key_right, key_left, key_up, key_down, key_jump,
        key_crouch, key_dive, key_walk, key_float, key_map;
key_right   = vk_right;
key_left    = vk_left;
key_up      = vk_up;
key_down    = vk_down;
key_crouch  = vk_down;
key_dive    = vk_down;
key_jump    = ord('S');
key_walk    = ord('A');
key_float   = ord('S');
key_map     = ord('C');

// Controller
globalvar device, dev_haxis, dev_h, dev_jump, dev_crouch,
        dev_dive, dev_walk, dev_float, dev_map;
device      = 0;
dev_haxis   = gp_axislh;
dev_h       = 0.3;
dev_jump    = gp_face1;
dev_crouch  = gp_face2;
dev_dive    = gp_face2;
dev_walk    = gp_shoulderrb;
dev_float   = gp_face1;
dev_map     = gp_face3;

// Abilities
globalvar canRunInAir, canVariableJump, canRunOnGround, canWalk, canSwim,
        canWallJump, canWallSlide, canDive, canCrouch, canFloat, canShield;

// Default abilities
canRunInAir         = true;
canVariableJump     = true;
canCrouch           = true;     // not implemented yet beyond using levers

// Earned abilities
canRunOnGround      = false;
canSwim             = false;
canWallJump         = false;
canWallSlide        = false;
canWalk             = false;    // not in use anymore
canDive             = false;
canFloat            = false;
canShield           = false;

// World Mechanics
globalvar powerOn, valvesOpen;

powerOn             = false;
valvesOpen          = false;

// World Map Grid
globalvar WorldMap;

WorldMap = defaultWorldMap();

// Level numbers
globalvar levelX, levelY, Area;
levelX = 8;
levelY = 8;
Area = abs(ds_grid_get(WorldMap, levelX, levelY));

// Saving
globalvar saveLevelX, saveLevelY, savePlayerX, savePlayerY;

saveLevelX          = 0;
saveLevelY          = 0;
savePlayerX         = 0;
savePlayerY         = 0; 

// Replay variables
globalvar showReplays, showDebug;

showReplays         = false;
showDebug           = false;

// Visual variables
globalvar lavaColor, lavaAlpha, torchColor;

lavaColor           = make_colour_rgb(145, 0, 0); //128
lavaAlpha           = 1;
torchColor          = make_colour_rgb(179, 151, 100); //c_orange

// Lighting
globalvar LightAmbience;

LightAmbience = 0.7;

// Attempt to load the game
globalvar LoadedGame;

LoadedGame = loadGame("save1.ini");

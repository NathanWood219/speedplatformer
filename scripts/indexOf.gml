///indexOf(string text, string phrase) - returns position of first occurance of phrase in text

for(i = 1; i <= string_length(argument0) - string_length(argument1); i++) {
    if(argument1 == string_copy(argument0, i, string_length(argument1))) {
        return i;
    }
}

return -1;

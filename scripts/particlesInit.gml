///particlesInit(sprite, index, width, height, scale, velocity, velocity+-, angle, angle+-, alpha)
// Nathan Wood
// 2 / 18 / 16
// 'obj_particle' or an equivalent is required to run this script

var spriteWidth, spriteHeight, spriteXOffset, spriteYOffset;

spriteWidth     = sprite_get_width(argument0);
spriteHeight    = sprite_get_height(argument0);
spriteXOffset   = x - sprite_get_xoffset(argument0);
spriteYOffset   = y - sprite_get_yoffset(argument0);

for(i = 0; i < spriteWidth / argument2; i++) {
    for(k = 0; k < spriteHeight / argument3; k++) {
    
        var obj, radians, ang, vel;
        
        obj = instance_create(spriteXOffset + (i * argument2),
            spriteYOffset + (k * argument3), obj_particle);
        
        obj.sprite  = argument0;
        obj.index   = argument1;
        obj.left    = i * argument2;
        obj.top     = k * argument3;
        obj.width   = argument2;
        obj.height  = argument3;
        obj.xscale  = argument4;
        obj.yscale  = argument4;
        
        ang         = argument7 + (random(argument8 * 2) - argument8);
        radians     = ang * (pi/180); 
        
        vel         = argument5 + (random(argument6 * 2) - argument6);
        obj.hspd    = vel * cos(radians);
        obj.vspd    = vel * sin(radians);
        
        obj.alpha   = argument9;
        obj.depth   = depth;
    }
}



///buildGrid(string grid) - returns the newly built grid; must be a rectangle
// Specialized version: converts each token to its digits and negates the value

globalvar gridArray, grid;

// Get length of grid
gridArray = tokenizer(argument0, true);
grid = ds_grid_create(array_height_2d(gridArray), array_height_2d(gridArray));

for(i = 0; i < array_height_2d(gridArray); i++) {
    for(k = 0; k < array_length_2d(gridArray, i); k++) {
        ds_grid_add(grid, k, i, -real(gridArray[i, k]));
    }
}

return grid;

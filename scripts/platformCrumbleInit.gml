///platformCrumbleInit()

image_speed     = 0;
image_index     = 0;
image_alpha     = 1;
visible         = true;

crumbling       = false;

X_OFFSET_RANGE  = 16;
Y_OFFSET_RANGE  = 16;
xOffset         = 0;
yOffset         = 0;

CRUMBLE_TIME    = 60;
RESPAWN_TIME    = 300;
timer           = -1;

FADE_IN_RATE    = 0.05;

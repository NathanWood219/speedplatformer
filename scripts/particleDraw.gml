///particleDraw()
// Nathan Wood
// 2 / 19 / 16
// Put this in the DRAW event of 'obj_particle'

draw_sprite_general(sprite, index, left, top, width, height, x, y, xscale, yscale, angle, c_white, c_white, c_white, c_white, alpha);

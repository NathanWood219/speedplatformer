///saveGame(file)

if!(ShouldSaveGame) {
    exit;
}

// Open file
ini_open(argument0);

/*          Key Bindings           */

// Keyboard
ini_write_real("Bindings", "key_right", key_right);
ini_write_real("Bindings", "key_left", key_left);
ini_write_real("Bindings", "key_up", key_up);
ini_write_real("Bindings", "key_down", key_down);
ini_write_real("Bindings", "key_crouch", key_crouch);
ini_write_real("Bindings", "key_dive", key_dive);
ini_write_real("Bindings", "key_jump", key_jump);
ini_write_real("Bindings", "key_walk", key_walk);
ini_write_real("Bindings", "key_float", key_float);
ini_write_real("Bindings", "key_map", key_map);

// Controller
ini_write_real("Bindings", "device", device);
ini_write_real("Bindings", "dev_haxis", dev_haxis);
ini_write_real("Bindings", "dev_h", dev_h);
ini_write_real("Bindings", "dev_jump", dev_jump);
ini_write_real("Bindings", "dev_crouch", dev_crouch);
ini_write_real("Bindings", "dev_dive", dev_dive);
ini_write_real("Bindings", "dev_walk", dev_walk);
ini_write_real("Bindings", "dev_float", dev_float);
ini_write_real("Bindings", "dev_map", dev_map);

/*          Abilities              */

// Default
ini_write_real("Abilities", "canRunInAir", canRunInAir);
ini_write_real("Abilities", "canVariableJump", canVariableJump);
ini_write_real("Abilities", "canCrouch", canCrouch);

// Earned
ini_write_real("Abilities", "canRunOnGround", canRunOnGround);
ini_write_real("Abilities", "canSwim", canSwim);
ini_write_real("Abilities", "canWallJump", canWallJump);
ini_write_real("Abilities", "canWallSlide", canWallSlide);
ini_write_real("Abilities", "canWalk", canWalk);
ini_write_real("Abilities", "canDive", canDive);
ini_write_real("Abilities", "canFloat", canFloat);
ini_write_real("Abilities", "canShield", canShield);

/*          World                   */

// World Mechanics
ini_write_real("World", "powerOn", powerOn);
ini_write_real("World", "valvesOpen", valvesOpen);

// World Map Grid
ini_write_string("World", "WorldMap", ds_grid_write(WorldMap));

// Level Numbers
ini_write_real("World", "levelX", levelX);
ini_write_real("World", "levelY", levelY);

// Close file
ini_close();

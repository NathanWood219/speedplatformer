///movingObjDraw()

// Drawing the drop shadow and chain/rope
drawDropShadow();

if(swinging || pendulum) {
    if(object_index = obj_boulder) {
        
        draw_sprite_general(spr_rope, 0, 0, 5, RADIUS, 13, x, y, 1, 1,
            angle + 180, c_white, c_white, c_white, c_white, 0.7);
            
        draw_sprite_general(spr_rope, 0, 0, 5, RADIUS, 13, oMIDPOINT.x, oMIDPOINT.y, 1, 1,
            angle, c_white, c_white, c_white, c_white, 0.7);
            
    } else {
        draw_sprite_general(spr_chain, 0, 0, 24, RADIUS * 2, 48, x, y, 0.5, 0.5,
            angle + 180, c_white, c_white, c_white, c_white, 1);
            
        draw_sprite_general(spr_chain, 0, 0, 24, RADIUS * 2, 48, x, y, 0.5, -0.5,
            angle + 180, c_white, c_white, c_white, c_white, 1);
    }
}

// Drawing self
draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,draw_angle,c_white,image_alpha);

// Debugging
if(debug) {
    draw_set_color(c_black);
    draw_set_halign(fa_left);
    draw_set_font(fnt_debug);
    draw_text(0, 64, "Angle: " + string(angle) + "#pendVelocity: " + string(pendVelocity));
}

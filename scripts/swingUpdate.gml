///swingUpdate()

if(!powerOn) {
    exit;
}

radians = angle * (pi/180); 

x = oMIDPOINT.x + (RADIUS * cos(radians));
y = oMIDPOINT.y - (RADIUS * sin(radians));


if(object_index == obj_laser_cannon) {
    image_angle = point_direction(x, y, oMIDPOINT.x, oMIDPOINT.y) + 180;
} else {
    angle -= SPIN_SPEED;
}

///playerDead()

if(dead || room = level_0_0) {
    exit;
}

vspd = 0; hspd = 0;
output = argument0;
dead = true;
vibrateController(0.8, 30, true);
image_alpha = 0;

resetAbilities();
resetWorldStates();

particlesInit(sprite_index, image_index, sprite_width / 4, sprite_height / 4, 1, 5, 2, 0, 180, 1);

alarm[2] = 40;  // time spent dead

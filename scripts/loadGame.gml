///loadGame(file) - returns true if successfully loaded

if!(file_exists(argument0)) {
    return false;
}

// Open file
ini_open(argument0);

/*          Key Bindings           */

// Keyboard
key_right       = ini_read_real("Bindings", "key_right", vk_right);
key_left        = ini_read_real("Bindings", "key_left", vk_left);
key_up          = ini_read_real("Bindings", "key_up", vk_up);
key_down        = ini_read_real("Bindings", "key_down", vk_down);
key_crouch      = ini_read_real("Bindings", "key_crouch", vk_down);
key_dive        = ini_read_real("Bindings", "key_dive", vk_down);
key_jump        = ini_read_real("Bindings", "key_jump", ord('S'));
key_walk        = ini_read_real("Bindings", "key_walk", ord('A'));
key_float       = ini_read_real("Bindings", "key_float", ord('S'));
key_map         = ini_read_real("Bindings", "key_map", ord('C'));

// Controller
device          = ini_read_real("Bindings", "device", 0);
dev_haxis       = ini_read_real("Bindings", "dev_haxis", gp_axislh);
dev_h           = ini_read_real("Bindings", "dev_h", 0.3);
dev_jump        = ini_read_real("Bindings", "dev_jump", gp_face1);
dev_crouch      = ini_read_real("Bindings", "dev_crouch", gp_face2);
dev_dive        = ini_read_real("Bindings", "dev_dive", gp_face2);
dev_walk        = ini_read_real("Bindings", "dev_walk", gp_shoulderrb);
dev_float       = ini_read_real("Bindings", "dev_float", gp_face1);
dev_map         = ini_read_real("Bindings", "dev_map", gp_face3);

/*          Abilities              */

// Default
canRunInAir     = ini_read_real("Abilities", "canRunInAir", true);
canVariableJump = ini_read_real("Abilities", "canVariableJump", true);
canCrouch       = ini_read_real("Abilities", "canCrouch", true);

// Earned
canRunOnGround  = ini_read_real("Abilities", "canRunOnGround", false);
canSwim         = ini_read_real("Abilities", "canSwim", false);
canWallJump     = ini_read_real("Abilities", "canWallJump", false);
canWallSlide    = ini_read_real("Abilities", "canWallSlide", false);
canWalk         = ini_read_real("Abilities", "canWalk", false);
canDive         = ini_read_real("Abilities", "canDive", false);
canFloat        = ini_read_real("Abilities", "canFloat", false);
canShield       = ini_read_real("Abilities", "canShield", false);

/*          World                   */

// World Mechanics
powerOn         = ini_read_real("World", "powerOn", false);
valvesOpen      = ini_read_real("World", "valvesOpen", false);

// World Map Grid
ds_grid_read(WorldMap, ini_read_string("World", "WorldMap", ""));

// Level Numbers
levelX          = ini_read_real("World", "levelX", 8);
levelY          = ini_read_real("World", "levelY", 8);
Area            = updateArea();

// Close file
ini_close();
return true;

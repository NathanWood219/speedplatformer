if(x > room_width) {
    levelX++;
    x = 0 ;
} else if(x < 0) {
    levelX--;
    x = room_width;
} else if(y > room_height) { 
    levelY++;
    y = 0;
} else if(y < 0) {
    levelY--;
    y = room_height;
}

updateArea();

if(!room_exists(asset_get_index("level_" + string(levelX) + "_" + string(levelY)))) {
    room_goto(level_empty);
} else {
    room_goto(asset_get_index("level_" + string(levelX) + "_" + string(levelY)));
}

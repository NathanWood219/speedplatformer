///physicsInit(velocity, angle)

physics = true;
depth = 25;

// Basic movement
S_GRAVITY       = 0.8;      //Accel. due to gravity (pixels/step*step)
S_GROUND_FRIC   = 0.4;      //Friction on the ground (pixels/step*step)
S_AIR_FRIC      = 0.1;      //Friction in the air (pixels/step*step)
S_MAX_H         = 16;       //Max horizontal speed
S_MAX_V         = 12;       //Max vertical speed
S_SLOPE_SLOW    = 0.5;      //Decceleration while climbing slopes

// Water movement
S_WATER_GRAVITY = 0.25;     //Accel. due to gravity in water
S_WATER_FRIC    = 1.2;      //Friction in the water
S_WATER_MAX_H   = 6;        //Max horizontal speed in water
S_WATER_MAX_V   = 3;        //Max vertical speed in water

// Initial velocity
radians = argument1 * (pi/180); 

hspd = argument0 * cos(radians);
vspd = argument0 * sin(radians);

prevVspd = vspd;

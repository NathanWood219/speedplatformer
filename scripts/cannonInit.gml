///cannonInit(object, scale, velocity, angle, interval, initDelay, usePhysics)

firing          = true;

FIRING_OBJECT   = argument0;
FIRING_SCALE    = argument1;
FIRING_VELOCITY = argument2;
FIRING_ANGLE    = argument3;
FIRING_INTERVAL = argument4;
firingTimer     = argument5;
FIRING_PHYSICS  = argument6;

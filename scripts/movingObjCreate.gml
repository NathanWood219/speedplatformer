///movingObjCreate(ROT_SPEED)
event_inherited();

ROT_SPEED = argument0;
draw_angle = 0;

image_speed = 0;

hspd = 0;
vspd = 0;
lastHspd = 1;
lastVspd = 1;

TURN_RATE = 0.05;
justTurned = false;
turning = false;
lastMarker = 0;

physics = false;

angle = 0;

swinging = false;
lockedSwinging = false;

pendulum = false;
PEND_GRAVITY = 0.1;
INIT_PEND_VELOCITY = 0;
MAX_PEND_VELOCITY = 0;
pendVelocity = 0;

tether = false;

moving = false;
followingWall = false;
followSpeed = 0;

firing = false;
projectile = false;

// WATER
lqd     = 0;
prvlqd  = 0;
slqd    = 0;
sprvlqd = 0;
splashdelay = 4;

debug = false;

///moveInit(velocity, angle)
radians = argument1 * (pi/180); 

hspd = argument0 * cos(radians);
vspd = argument0 * sin(radians);

lastHspd = hspd;
lastVspd = vspd;

moving = true;

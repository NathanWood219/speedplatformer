///playerVariables() - Sets up all the initial player constants and variables

// Basic movement
S_GRAVITY       = 0.8;      //Accel. due to gravity (pixels/step*step)
S_RUN_ACCEL     = 0.6;      //Accel. from running on ground (pixels/step*step)
S_RUN_FRIC      = 1.5;      //Friction on the ground (pixels/step*step)
S_AIR_ACCEL     = 0.6;      //Accel. from running in the air (pixels/step*step)
S_AIR_FRIC      = 0.8;      //Friction in the air (pixels/step*step)
S_JUMP_SPEED    = -16;
S_DJUMP_SPEED   = 0;        //Double jump speed
S_MAX_H         = 9;        //Max horizontal speed [x/2 = crawling, x = walking]
S_MAX_RUN       = 16;       //Max running speed
S_MAX_V         = 16;       //Max vertical speed
S_SLOPE_SLOW    = 0.2;      //Decceleration while climbing slopes

// Basic movement
/*
S_GRAVITY       = 0.8;      //Accel. due to gravity (pixels/step*step)
S_RUN_ACCEL     = 0.4;      //Accel. from running on ground (pixels/step*step)
S_RUN_FRIC      = 1.2;      //Friction on the ground (pixels/step*step)
S_AIR_ACCEL     = 0.4;      //Accel. from running in the air (pixels/step*step)
S_AIR_FRIC      = 0.5;      //Friction in the air (pixels/step*step)
S_JUMP_SPEED    = -16;
S_DJUMP_SPEED   = 0;        //Double jump speed
S_MAX_H         = 8;        //Max horizontal speed [x/2 = crawling, x = walking]
S_MAX_RUN       = 14;       //Max running speed
S_MAX_V         = 16;       //Max vertical speed
S_SLOPE_SLOW    = 0.2;      //Decceleration while climbing slopes
*/
// Water movement
S_WATER_GRAVITY = 0.25;     //Accel. due to gravity in water
S_WATER_MAX_V   = 3;        //Max vertical speed in water
S_WATER_ACCEL   = 0.5;      //Accel. from swimming in water
S_WATER_FRIC    = 1.2;      //Friction in the water
S_WATER_MAX_H   = 6;        //Max horizontal speed in water
S_WATER_SWIM    = -8;       //Amount of vertical speed from swimming
S_WATER_DIVE_MAX= 8;        //Max dive speed in water

// WATER
lqd     = 0;
prvlqd  = 0;
slqd    = 0;
sprvlqd = 0;
splashdelay = 4;

// Abilities
S_DIVE_ACCEL    = 1.5;      //Dive acceleration
S_DIVE_MAX      = 20;       //Max dive speed
S_FLOAT_MAX     = 4;        //Max float speed
S_SLIDE_MAX     = 4;        //Max wallslide speed (vertical)
S_WALLJUMP_H    = 12;       //Walljump horizontal speed
S_WALLJUMP_V    = -16;      //Walljump vertical speed

//Animation stuff
image_speed = 0;

// Initial respawn point
saveLevelX      = levelX;
saveLevelY      = levelY;
savePlayerX     = x;
savePlayerY     = y;

// Loading ability states
saveAbilities();
saveWorldStates();
saved           = false;

//Abilities
can_jump        = true;
jump_timer      = 0;
candj           = false;
canflt          = true;
canTurnLever    = true;

//Player states
jumping         = false;
crouching       = false;
canwj           = true;
wjumping        = false;
wjumpDir        = 0;
floating        = false;
movement        = 0;        // 0 = standing, 1 = sliding (coming to a stop), 2 = skidding (changing directions), 3 = walking, 4 = running. 5 = swimming

// Other states
landed          = false;    // used for vibration
dead            = false;

justEnteredRoom = false;

vibrationStart  = 0;
vibrationAmount = 0;
vibrationTimer  = 0;
vibrationFade   = false;

// Wallsticking
STICK_TIME      = 3;
stickTimer      = STICK_TIME + 1;
canMove         = true;

// Drawing
output = "";
alpha = 0; // Flashing the screen

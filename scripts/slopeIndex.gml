///slopeIndex() - returns index value for slope type, 0 if no slope
amount = 2;

if(place_meeting(x,y+amount,obj_slopeL)) {
    return 1;
} else if(place_meeting(x,y+amount,obj_slopeR)) {
    return 2;
} else if(place_meeting(x,y+amount,obj_slopeL2)) {
    return 3;
} else if(place_meeting(x,y+amount,obj_slopeR2)) {
    return 4;
}

return 0;

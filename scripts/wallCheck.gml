///wallCheck(bool left, bool right, bool up, bool down) - each wall section to examine

// Static variables
DISTANCE = sprite_width;
OBJECT = object_index;

// Left
if(argument0 && !collision_line(x, y, x - DISTANCE, y, OBJECT, false, true)) {
    return false;
}

// Right
if(argument1 && !collision_line(x, y, x + DISTANCE, y, OBJECT, false, true)) {
    return false;
}

// Up
if(argument2 && !collision_line(x, y, x, y - DISTANCE, OBJECT, false, true)) {
    return false;
}

// Down
if(argument3 && !collision_line(x, y, x, y + DISTANCE, OBJECT, false, true)) {
    return false;
}

// Only returns true if all the conditions have been met
// Also set that the index has been selected
return true;

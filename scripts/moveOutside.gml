///moveOutside(object, pixelsPerStep)

angle = point_direction(x, y, argument0.x, argument0.y) + 180;
radians = angle * (pi/180);

x += argument1 * cos(radians);
y += argument1 * sin(radians);

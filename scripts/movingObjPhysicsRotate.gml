///movingObjPhysicsRotate() - put in BEGIN STEP
event_inherited();

if(!physics) {
    exit;
}

if(on_ground && abs(hspd) < 2.5) {
    if(!checkWall(x+1, y+1)) {
        hspd = 2.5;
    } else if(!checkWall(x-1, y+1)) {
        hspd = -2.5;
    }
}

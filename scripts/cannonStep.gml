///cannonStep()

if(firing) {
    if(firingTimer > 0) {
        firingTimer--;
    }
    
    if(firingTimer == 0) {
        with(instance_create(x, y, FIRING_OBJECT)) {
            image_xscale = other.FIRING_SCALE;
            image_yscale = other.FIRING_SCALE;
            
            if(other.FIRING_PHYSICS) {
                physicsInit(other.FIRING_VELOCITY, other.FIRING_ANGLE);
            } else {
                moveInit(other.FIRING_VELOCITY, other.FIRING_ANGLE);
            }
            
            projectile = true;
        }
        
        firingTimer = FIRING_INTERVAL;
    }
}


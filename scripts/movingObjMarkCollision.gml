///movingObjMarkCollision()

if(moving && !justTurned) {
    turning = true;
    
    if(lastHspd != 0) {
        hspd += -lastHspd / abs(lastHspd) * TURN_RATE;
    }
    
    if(lastVspd != 0) {
        vspd += -lastVspd / abs(lastVspd) * TURN_RATE;
    }
}

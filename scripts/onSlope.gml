///onSlope() - returns true if the player is on a slope
amount = 2;
return (place_meeting(x,y+amount,obj_slopeR) || place_meeting(x,y+amount,obj_slopeL)
    || place_meeting(x,y+amount,obj_slopeR2) || place_meeting(x,y+amount,obj_slopeL2));

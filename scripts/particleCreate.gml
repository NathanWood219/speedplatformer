///particleCreate()
// Nathan Wood
// 2 / 19 / 16
// Put this in the CREATE event of 'obj_particle'

// Constants
GRAVITY     = 0.4;          // Vertical acceleration
MAX_H       = 12;           // Max horizontal speed
MAX_V       = 12;           // Max vertical speed
FADE_RATE   = 0.05;         // Fading out rate before destroying (from 1 - 0)

// Movement
hspd        = 0;
vspd        = 0;
fric        = 0.8;

// Drawing
sprite      = 0;
index       = 0;
left        = 0;
top         = 0;
width       = 0;
height      = 0;
xscale      = 1;
yscale      = 1;
angle       = 0;
alpha       = 1;

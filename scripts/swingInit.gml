///swingInit(SPIN_SPEED, oMIDPOINT)

if(argument1 == 0) {
    oMIDPOINT = instance_nearest(x,y,obj_midpoint);
} else {
    oMIDPOINT = argument1;
}

RADIUS = distanceToObject(x,y,oMIDPOINT.x,oMIDPOINT.y);
SPIN_SPEED = argument0;
ROT_SPEED = -SPIN_SPEED*2;
angle = point_direction(oMIDPOINT.x,oMIDPOINT.y,x,y);
INIT_ANGLE = angle;

swinging = true;

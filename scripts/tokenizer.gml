///tokenizer(string text, boolean multipleLines) - returns array of elements from string

var array, char, startIndex;

startIndex = 1;
row = 0;
column = 0;

for(i = 1; i <= string_length(argument0); i++) {
    char = string_char_at(argument0, i);
    
    if(argument1 && char == "#") {
        array[row, column] = string_copy(argument0, startIndex, i - startIndex + 1);
        
        startIndex = i + 1;
        row++;
        column = 0;
        
    } else if(char == " ") {
        if(startIndex != i) {
            if(argument1) {
                array[row, column] = string_copy(argument0, startIndex, i - startIndex + 1);
            } else {
                array[column] = string_copy(argument0, startIndex, i - startIndex + 1);
            }
            
            column++;
        }
        
        startIndex = i + 1;
    }
}

return array;

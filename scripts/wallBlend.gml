///wallBlend() - detects adjacent walls and blends accordingly
// prev. versions available in Hindsight, Flashback
// now uses wallCheck() to verify block types

// order must remain the same (priority)

if(object_index != obj_wall) {
    if(object_index == obj_slopeR) {
        sprite_index = spr_slopeR;
    } else if(object_index == obj_slopeL) {
        sprite_index = spr_slopeL;
    } else if(object_index == obj_slopeR2) {
        sprite_index = spr_slopeR2;
    }  else if(object_index == obj_slopeL2) {
        sprite_index = spr_slopeL2;
    }
    
    exit;
}

image_speed = 0;

if(wallCheck(true, true, true, true)) {                         // inside block
    image_index = 16;
} else if(wallCheck(true, true, false, false)) {                // 2 sided walls
    image_index = 10;
} else if(wallCheck(false, false, true, true)) {
    image_index = 9;
} else if(wallCheck(true, true, true, false)) {                 // 1 sided walls
    image_index = 11;
} else if(wallCheck(true, true, false, true)) {
    image_index = 13;
} else if(wallCheck(false, true, true, true)) {
    image_index = 12;
} else if(wallCheck(true, false, true, true)) {
    image_index = 14;
} else if(wallCheck(true, false, true, false)) {                // corner walls
    image_index = 5;
} else if(wallCheck(true, false, false, true)) {
    image_index = 8;
} else if(wallCheck(false, true, true, false)) {
    image_index = 6;
} else if(wallCheck(false, true, false, true)) {
    image_index = 7;
} else if(wallCheck(false, false, true, false)) {               // 3 sided walls
    image_index = 1;
} else if(wallCheck(false, false, false, true)) {
    image_index = 3;
} else if(wallCheck(true, false, false, false)) {
    image_index = 4;
} else if(wallCheck(false, true, false, false)) {
    image_index = 2;
} else {                                                        // solid 1 block wall
    image_index = 0;
}


///lockedSwingInit(oMIDPOINT)

if(argument0 == 0) {
    oMIDPOINT = instance_nearest(x,y,obj_midpoint);
} else {
    oMIDPOINT = argument0;
}

RADIUS = distanceToObject(x,y,oMIDPOINT.x,oMIDPOINT.y);
INIT_ANGLE = point_direction(x, y, oMIDPOINT.x, oMIDPOINT.y);
angle = oMIDPOINT.image_angle + INIT_ANGLE;

lockedSwinging = true;

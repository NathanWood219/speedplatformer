///pendInit(INIT_PEND_VELOCITY, oMIDPOINT)

if(argument1 == 0) {
    oMIDPOINT = instance_nearest(x,y,obj_midpoint);
} else {
    oMIDPOINT = argument1;
}

RADIUS = distanceToObject(x,y,oMIDPOINT.x,oMIDPOINT.y);

INIT_PEND_VELOCITY = argument0;
pendVelocity = INIT_PEND_VELOCITY;

angle = point_direction(x, y, oMIDPOINT.x, oMIDPOINT.y) + 180;

pendulum = true;

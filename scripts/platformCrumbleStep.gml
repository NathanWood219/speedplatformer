///platformCrumbleStep()

if(timer > 0) {
    timer--;
}

if(image_alpha < 1) {
    image_alpha += FADE_IN_RATE;
}

if(image_alpha > 1) {
    image_alpha = 1;
}

if(!crumbling && (place_meeting(x, y-1, obj_player) || place_meeting(x-1, y, obj_player) || place_meeting(x+1, y, obj_player))) {
    crumbling = true;
    timer = CRUMBLE_TIME;
}

if(crumbling) {
    xOffset = random(X_OFFSET_RANGE) - (X_OFFSET_RANGE / 2);
    yOffset = random(Y_OFFSET_RANGE) - (Y_OFFSET_RANGE / 2);
    
    if(timer == 0) {
        image_index = 1;
        crumbling = false;
        timer = RESPAWN_TIME;
        
        xOffset = 0;
        yOffset = 0;
        
        particlesInit(sprite_index, 0, 32, 32, 1, 3, 1, 0, 180, 1);
    }
} else {
    if(timer == 0) {
        timer = -1;
        image_index = 0;
        image_alpha = 0;
    }
}

///clearSurface(surface, color, alpha)
if(surface_exists(argument0)) {
    surface_set_target(argument0);
    draw_clear_alpha(argument1, argument2);
    surface_free(argument0);
    surface_reset_target();
}

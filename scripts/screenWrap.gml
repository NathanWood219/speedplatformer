width = room_width + sprite_width;
height = room_height + sprite_height;

if(x>width) {
    x -= width + sprite_width;
} else if(x<-sprite_width) {
    x += width + sprite_width;
}

if(y>height) {
    y -= height + sprite_height;
} else if(y<-sprite_height) {
    y += height + sprite_height;
}

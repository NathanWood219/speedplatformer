///movingObjectStep(object_index)

if(physics) {
    exit;
}

if(!moving && !pendulum && !lockedSwinging && object_index != obj_midpoint) {
    draw_angle += ROT_SPEED;
}
    
if(moving && powerOn) {
    x += hspd;
    y += vspd;
    
    if(lastHspd != 0) {
        draw_angle -= hspd / abs(lastHspd / 2.5);
    } else {
        draw_angle -= vspd / abs(lastVspd / 2.5);
    }
}

if(lockedSwinging && powerOn) {
    angle = oMIDPOINT.oTETHER.draw_angle + INIT_ANGLE;
    
    draw_angle = point_direction(x, y, oMIDPOINT.x, oMIDPOINT.y);
    
    radians = angle * (pi/180);
    
    x = oMIDPOINT.x + (RADIUS * cos(radians));
    y = oMIDPOINT.y - (RADIUS * sin(radians));
}

if(swinging && powerOn) {
    if(object_index == obj_laser_cannon) {
        draw_angle = point_direction(x, y, oMIDPOINT.x, oMIDPOINT.y) + 180;
    } else {
        angle -= SPIN_SPEED;
    }
    
    radians = angle * (pi/180);
    
    x = oMIDPOINT.x + (RADIUS * cos(radians));
    y = oMIDPOINT.y - (RADIUS * sin(radians));
}

if(pendulum) {
    
    if(abs(angle) < 90 || abs(angle) > 270) {            // on the right side
        pendVelocity -= PEND_GRAVITY;
    } else if(abs(angle) > 90 && abs(angle) < 270) {     // on the left side
        pendVelocity += PEND_GRAVITY;
    }
    
    angle += pendVelocity;
    angle = angle % 360;
    
    if(object_index == obj_laser_cannon) {
        draw_angle = point_direction(x, y, oMIDPOINT.x, oMIDPOINT.y) + 180;
    } else {
        draw_angle = angle * 2.5;;
    }
    
    radians = angle * (pi/180);
    
    x = oMIDPOINT.x + (RADIUS * cos(radians));
    y = oMIDPOINT.y - (RADIUS * sin(radians));
}

if(tether and object_index != argument0) {
    x = oTETHER.x;
    y = oTETHER.y;
}

if(moving && turning && !justTurned) {
    if(lastHspd != 0) {
        if(abs(hspd) < abs(lastHspd)) {
            hspd += -lastHspd / abs(lastHspd) * TURN_RATE;
        } else {
            hspd = -lastHspd;
        }
    }
    
    if(lastVspd != 0) {
        if(abs(vspd) < abs(lastVspd)) {
            vspd += -lastVspd / abs(lastVspd) * TURN_RATE;
        } else {
            vspd = -lastVspd;
        }
    }
    
    if(hspd == -lastHspd && vspd == -lastVspd) {
        lastHspd = hspd;
        lastVspd = vspd;
        
        turning = false;
        justTurned = true;
    }
}

if(justTurned && !projectile && !place_meeting(x,y,obj_marker)) {
    justTurned = false;
}

if(projectile && (checkWall(x, y) || outsideRoom())) {
    particlesInit(sprite_index, image_index, sprite_width / 4, sprite_height / 4, 1, 3, 1, 0, 180, 1);
    instance_destroy();
}

///movingObjPhysics() - put in STEP
if(!physics) {
    exit;
}

var fric;

if(in_water) {
    fric = S_WATER_FRIC;
} else if(on_ground) {
    fric = S_GROUND_FRIC;
} else {
    fric = S_AIR_FRIC;
}

if(hspd != 0) {
    draw_angle -= hspd;
} else {
    draw_angle += vspd / 2;
}

if(on_ground) {
    var slope, slopeDir, rads;
    
    slope = slopeIndex();
    
    switch(slope) {
        case 1: slopeDir = 315; break;
        case 2: slopeDir = 225; break;
        case 3: slopeDir = 340; break;
        case 4: slopeDir = 200; break;
        default: slopeDir = 0;  break;
    }
    
    rads = slopeDir * pi/180;
    
    if(slope != 0) {
        var rampSpeed;
        
        rampSpeed = prevVspd * cos(rads);
        
        if((hspd > 0 && rampSpeed < 0) || (hspd < 0 && rampSpeed > 0)) {
            hspd = approach(hspd, 0, fric);
        } else {
            hspd = approach(hspd, prevVspd * cos(rads), fric);
        }
    } else if(abs(hspd)>0) {
        hspd = approach(hspd, 0, fric);
    }
} else {
    if(in_water) {
        vspd = approach(vspd, S_WATER_MAX_V, S_WATER_GRAVITY);
    } else {
        vspd = approach(vspd, S_MAX_V, S_GRAVITY);
    }
}

// Limit speeds
if(hspd > S_MAX_H) {
    hspd = S_MAX_H;
} else if(hspd < -S_MAX_H) {
    hspd = -S_MAX_H;
}

if(vspd > S_MAX_V) {
    vspd = S_MAX_V;
} else if(vspd < -S_MAX_V) {
    vspd = -S_MAX_V;
}


if(projectile && hspd == 0 && vspd == 0) {
    particlesInit(sprite_index, image_index, sprite_width / 4, sprite_height / 4, 1, 3, 1, 0, 180, 1);
    instance_destroy();
}
